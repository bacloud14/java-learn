package artifact_id;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import io.github.cdimascio.dotenv.Dotenv;

public class Test {

	public static void main(String[] args) throws IOException, InterruptedException {

		Dotenv dotenv = Dotenv.load();

		SString temp = new SString("test");
		Set<String> tags = new HashSet<String>();
		tags.addAll(Arrays.asList(new String[] { "tag1_a", "tag1_b" }));

		SString temp2 = new SString("test2");
		Set<String> tags2 = new HashSet<String>();
		tags2.addAll(Arrays.asList(new String[] { "tag1_a", "tag1_b" }));

		Tagged tag1 = new Tagged(tags, temp);
		Tagged tag2 = new Tagged(tags2, temp2);

		try {
			Registery.addSstring(tag1);
			Registery.addSstring(tag2);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		System.out.println(Registery.print());
//		temp.addMyEventListener(() -> {
//			System.out.println("client: emptied");
//		});

		temp.set("");
		temp.set("");

		System.out.print(temp.str);

		/****************************************************************************/
		
		WatchService watchService = FileSystems.getDefault().newWatchService();
		File file = new File("src/main/resources");
		String absolutePath = file.getAbsolutePath();

		Path path = Paths.get(absolutePath);

		path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
				StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);

		WatchKey key;
		while ((key = watchService.take()) != null) {
			for (WatchEvent<?> event : key.pollEvents()) {
				dotenv = Dotenv.load();
				String str = dotenv.entries().stream().map((entry) -> entry.toString())
						.collect(Collectors.joining("\n"));
				System.out.println(str);
				System.out.println(
						"Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
			}
			key.reset();
		}
	}

}
