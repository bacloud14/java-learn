package artifact_id;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public class SString implements EventListener {
	String str;
	private List<EmptiedListener> listners;

	public SString(String str) {
		this.listners = new ArrayList<EmptiedListener>();

		if (str.isBlank()) {
			this.str = "__" + java.util.UUID.randomUUID().toString();
			listners.forEach((el) -> el.onMyEvent());
		} else {
			this.str = str;
		}

		this.addMyEventListener(() -> {
			System.out.println("SString: emptied");
		});

	}

	public String get() {
		return str;
	}

	public void set(String str) {
		if (str.isBlank()) {
			this.str = "__" + java.util.UUID.randomUUID().toString();
			listners.forEach((el) -> el.onMyEvent());
		} else {
			this.str = str;
		}

	}

	int length() {
		return str.length();
	}

	public void addMyEventListener(EmptiedListener evtListener) {
		this.listners.add(evtListener);
	}

}