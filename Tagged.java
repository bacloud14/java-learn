package artifact_id;

import java.util.Set;

public class Tagged {

	public Tagged(Set<String> tags, SString str) {
		super();
		this.tags = tags;
		this.str = str;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public SString getStr() {
		return str;
	}

	public void setStr(SString str) {
		this.str = str;
	}

	private Set<String> tags;
	private SString str;
}
