package artifact_id;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Registery {
	private static List<Tagged> registery = new ArrayList<Tagged>();
	private static Set<String> uniques = new HashSet<String>();
	private static boolean stable = true;

	public static List<Tagged> getSstrings() {
		return registery;
	}

	public static String print() {
		String listString = Registery.registery.stream()
				.map((tagged) -> tagged.getStr().get() + ": " + tagged.getTags().toString())
				.collect(Collectors.joining("\n"));
		listString += "\n==============================\n";
		return listString;
	}

	public static void addSstring(Tagged tStr) throws Exception {
		Set<String> tags = tStr.getTags();
		boolean exists = Registery.registery.stream()
				.anyMatch((tagged) -> tagged.getTags().equals(tags));

		if (exists) {
			Registery.stable = false;
			throw new Exception(
					"Registery unstable: tag does exist already. " + tStr.getTags().toString());
		}

		Registery.registery.add(tStr);

		uniques.addAll(tStr.getTags());
	}
}
